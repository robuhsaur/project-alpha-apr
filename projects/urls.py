from django.urls import path
from projects.views import (
    ProjectDetailView,
    ProjectListView,
    create_project,
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", create_project, name="create_project"),
]
