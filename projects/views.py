from django.shortcuts import redirect, render
from django.views.generic.list import ListView
from projects.forms import ProjectCreateForm
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "projects"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "projects"


def create_project(request):
    context = {}
    form = ProjectCreateForm(request.POST or None)
    if form.is_valid():
        project = form.save()
        return redirect("show_project", pk=project.pk)
    context = {"form": form}
    return render(request, "projects/create.html", context)
