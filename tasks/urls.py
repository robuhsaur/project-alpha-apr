from django.urls import path
from tasks.views import TaskListView, TaskUpdateView, create_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
]
