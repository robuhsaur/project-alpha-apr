from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from tasks.forms import TaskCreateForm
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView


# Create your views here.


@login_required
def create_task(request):
    context = {}
    form = TaskCreateForm(request.POST or None)
    if form.is_valid():
        tasks = form.save()
        return redirect("show_project", pk=tasks.project.pk)
    context = {"form": form}
    return render(request, "tasks/create.html", context)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"
    context_object_name = "tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = "show_my_tasks"

    def get_success_url(self):
        return reverse("show_my_tasks")
